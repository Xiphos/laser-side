import sys, os, discord, random, re
import urllib.request
import json
client = discord.Client()
token = 'TOKEN'

@client.event
async def on_ready():
    print("What is up")
    await client.change_presence(game=discord.Game(name="Scanning for gamers"))

@client.event
async def on_message(message):

    if message.author == client.user:
        return
    
    if message.content == "gamer located":
        await client.send_message(message.channel, "ELIMINATING GAMER")
    
    if message.content == "gamer status":
        msg = getStatus()
        await client.send_message(message.channel, msg)
    
    if message.content.startswith("gamer"):
        if len(message.mentions) == 0:
            await client.send_message(message.channel, "No gamers specified")
        else:
            recorded_gamers = []
            record = ''
            new_ids = []
            new_gamers = []
            new = ''
            for member in message.mentions:
                with open('gamers.txt', 'r+') as f:
                    ids = [line.rstrip() for line in f.readlines()]
                    print(ids)
                    if member.id in ids:
                        recorded_gamers.append(member.name)
                    else:
                        new_ids.append(member.id)
                        new_gamers.append(member.name)
            
            if len(recorded_gamers) == 0 :
                record = ""
            else: 
                record = "Found Existing Gamers: {}".format(" ".join(recorded_gamers))

            if len(new_gamers) == 0:
                new = ""
            else:
                new = "Newly Registered Gamers: {}".format(" ".join(new_gamers))
                with open('gamers.txt', 'a') as f:
                    for i in new_ids:
                        f.write(i + "\n")

            msg = record + "\n" + new

            await client.send_message(message.channel, msg)

    if message.content.startswith("isgamer"):
        if len(message.mentions) == 0:
            await client.send_message(message.channel, "No potential gamers specified")
        else:
            clean = []
            registered = []
            with open('gamers.txt', 'r+') as f:
                ids = [line.rstrip() for line in f.readlines()]
                print(ids)
                for member in message.mentions:
                    if member.id in ids:
                        registered.append(member.name)
                    else:
                        clean.append(member.name)
            
            if len(registered) == 0:
                await client.send_message(message.channel, "All users are clean")
            else:
                await client.send_message(message.channel, "Registered gamers found: {}".format(" ".join(registered)))


def getStatus():
    response = urllib.request.urlopen("http://laserstatus.mtxn.ca/laserfiles/laserLab.html")
    html = response.read().decode('utf-8')
    html = html.split('\n')
    
    partsOfMessage = {
        'lastupdate': ['', re.compile(r'Last Updated: (.*) ===')],
        'l1breaks': ['', re.compile(r'Laser 1 was broken (\d*) times?')],
        'l2breaks': ['', re.compile(r'Laser 2 was broken (\d*) times?')],
        'entrances': ['', re.compile(r'(\d) objects? entered the room')],
        'exits': ['', re.compile(r'(\d) objects? exited the room')],
    }

    for line in html:
        for part in partsOfMessage.keys():
            pattern = partsOfMessage[part][1]
            matches = re.search(pattern, line)
            if matches is not None:
                if matches.groups()[0] is not None:
                    partsOfMessage[part][0] = matches.groups()[0]

    valueDict = {}
    for arg in partsOfMessage:
        valueDict[arg] = partsOfMessage[arg][0]

    message = """**Success!** Got the following status from request:
**Laser breaks**: {} and {}
**Entrances**: {}
**Exits**: {}
**Last updated**: {}
    """.format(
        valueDict['l1breaks'], valueDict['l2breaks'],
        valueDict['entrances'],
        valueDict['exits'],
        valueDict['lastupdate']
    )

    return message

client.run(token)
