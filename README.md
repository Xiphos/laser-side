<h1> Introduction </h1>

This is a companion project to my [Laser Lab project](https://gitlab.com/Xiphos/laser-lab). Just like that project, updates regarding my progress live at [mtxn](http://www.mtxn.ca).

<h2>Table of Contents</h2>  

1.  [Overview of Features](#overview)  
    I.  [Using the Debugging Interface](#howtodebug)  

<a name='overview'></a>
<h1>Overview of Features</h1>

Currently the main feature of this project is `debug.py`. It provides a sort of scripting language for debugging and testing the laserLab project. It is placed in the `/usr/bin/` of the Raspberry Pi Model B so it can simply be executed with `debugLaser`, which will begin a scripting stream for you to enter commands.  

<a name='howtodebug'></a>
<h2>Using the Debugging Interface</h2>
Commands are entered as one line, consisting of the action followed by an argument. The list of commands is as follows:  

| <b>Action</b> 	| <b>Argument</b> 	| <b>Result</b> 	|
|------------------------------------------	|-----------------	|------------------------------------------------------	|
| `on` 	| laser (1,2) 	| turn the laser on 	|
| `off` 	| laser (1,2) 	| turn the laser off 	|
| `t, tog, toggle` 	| laser (1,2) 	| toggle the laser's state 	|
| `exit` 	| time (ms) 	| simulate an exit with a given time between steps  	|
| `enter` 	| time (ms) 	| simulate an entrance with a given time between steps 	|
| `execute, exec, runFile, run, interpret` 	| filepath 	| run a script (`*.lsc`) at the given filepath 	|  

A script file can also include custom wait times between steps. To do this, simply enter a number on a line which represents how many milliseconds to wait. Breakdown in functionality occurs at around `10ms`, so the default sleep time is `100ms`, you should try to keep times above this. As mentioned in the table, script files should have a `.lsc` extension, though this is technically optional it makes it easier to remove/organize scripts with a `*.lsc` pattern.