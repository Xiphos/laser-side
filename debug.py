#!/usr/bin/python
import RPi.GPIO as GPIO 
import re, sys, os, time
import keyboard
from termcolor import colored

class LaserState:
    states = [] ##Holds 1s and 0s
    pins = [] ##Holds the pin numbers
    stringRep = '||'

    commandMap = {
        ##On/off
        'on': 'turnOn',
        'off': 'turnOff',

        ##Multiple toggle aliases
        't': 'toggle',
        'tog': 'toggle',
        'toggle': 'toggle',

        ##'macro' functions
        'exit': 'simExit',
        'enter': 'simEnter',

        ##Run from text script
        'exec': 'runFile',
        'runFile': 'runFile',
        'run': 'runFile',
        'interpret': 'runFile',
        'execute': 'runFile',
    }

    def __init__(self, laser1pin, laser2pin):
        self.states = [1,1]
        self.pins = [laser1pin, laser2pin]
        GPIO.setup(laser1pin, GPIO.OUT)
        GPIO.setup(laser2pin, GPIO.OUT)
    
    def printState(self):
        for char in self.stringRep:
            col = 'red'
            if char == '|':
                col = 'green'
            
            print colored(char, col),
        print ##End line
    
    def action(self, command, arg):
        ##Takes in a command like 'on' or 'off' etc. and an argument,
        ##Parses it with the commandMap dict

        ##Just skip invalid keys
        if not command in self.commandMap.keys():
            return

        cmd = self.commandMap[command]

        ##If the command exists, do it
        if(cmd):
            func = getattr(self, cmd)
            func(arg)

    def runFile(self, path):
        #Reads the file as a script
        speed = 250.0 / 1000.0
        try:
            f = open(path, 'r')
        except IOError:
            print("Could not locate the script")
            return
        
        commandPattern = re.compile(r'^(\w*)\s(.*)$')
        ##Initialize the interface
        for req in f.readlines():
            req.rstrip()
            ##Break
            if not req: 
                print "Read end of script"
                f.close()
                break #Read till EOF

            ##If the command matches the pattern ([command] [arg])
            match = commandPattern.match(req)
            if match:
                command = match.groups()[0]
                arg = match.groups()[1]
                self.action(command, arg)
            
            ##Allow scripts to choose how they want to sleep
            sleepMatch = re.compile(r'^(\d*)$').match(req)
            if sleepMatch:
                time.sleep( float(sleepMatch.groups()[0]) / 1000.0 )
                print "Slept"
            else:
                time.sleep(speed) ##Otherwise sleep with the default

        f.close()


    ##We will only update when something is changed
    def toggle(self, laser):
        ##If 1 -> 0 If 0 -> 1
        laser = int(laser)
        laser -= 1
        self.states[laser] = 1 - self.states[laser]
        self.update()

    def turnOff(self, laser):
        laser = int(laser)
        laser -= 1
        self.states[laser] = 0
        self.update()

    def turnOn(self, laser):
        laser = int(laser)
        laser -= 1
        self.states[laser] = 1
        self.update()

    def update(self):
        ##Write the states
        oldRep = self.stringRep
        self.stringRep = ''
        for pin, state in zip(self.pins, self.states):
            if(state == 1):
                self.stringRep += '|'
            elif(state == 0):
                self.stringRep += 'x'
            GPIO.output(pin, state)

        if(self.stringRep != oldRep):
            self.printState() ##only print new states
    
    def simEnter(self, wait):
        #Runs a basic exit script with a given timeout in ms
        wait = float(wait)
        wait /= 1000.0
        self.turnOn(1)
        self.turnOn(2)  
        time.sleep(wait)#||
                        #||
        self.turnOff(1) #x|
        time.sleep(wait)#x|
        self.turnOff(2) #xx
        time.sleep(wait)#xx
        self.turnOn(1)  #|x
        time.sleep(wait)#|x
        self.turnOn(2)  #||
        print "Finished exit script with timeout " + str(wait*1000) + "ms."
    
    def simExit(self, wait):
        #Runs a basic entrance script with a given timeout in ms
        wait = float(wait)
        wait /= 1000.0
        self.turnOn(1)
        self.turnOn(2)  
        time.sleep(wait)#||
                        #||
        self.turnOff(2) #|x
        time.sleep(wait)#|x
        self.turnOff(1) #xx
        time.sleep(wait)#xx
        self.turnOn(2)  #x|
        time.sleep(wait)#x|
        self.turnOn(1)  #||
        print "Finished entrance script with timeout " + str(wait*1000) + "ms."

if __name__ == "__main__":
    ##Set GPIO Stuff
    GPIO.setmode(GPIO.BOARD)
    GPIO.setwarnings(False)
    state = LaserState(11, 7) ##The arguments are the pin numbers
    exitAliases = [        ##Exit
        'done',
        'quit',
        'exit',
        'end',
    ]

    commandPattern = re.compile(r'^(\w*)\s(.*)$')
    ##Initialize the interface
    try:
        while(1):
            ##> symbol to show that we are accepting a command
            print('>'),
            req = raw_input()

            if req in exitAliases:
                sys.exit()

            ##If the command matches the pattern ([command] [arg])
            match = commandPattern.match(req)
            if(match):
                command = match.groups()[0]
                arg = match.groups()[1]
                state.action(command, arg)
    except KeyboardInterrupt:
        print
        print "Got keyboard interrupt. Closing..."
        sys.exit()
